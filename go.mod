module gitea.com/go-chi/binding

go 1.21

require (
	github.com/go-chi/chi/v5 v5.0.4
	github.com/goccy/go-json v0.9.5
	github.com/stretchr/testify v1.3.0
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
